/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Geri
 */

public class Price {
    
    
    private double modified;
    private String rate;
    
    public Price() {
    }

    public Price(int original, double rate_num) {
        this.modified = original * rate_num;
        this.rate = original < 0 ? "-" : "+" + (int)((rate_num * 100) - 100) + "%";
    }

    @XmlElement(name = "offer")
    public double getModified() {
        return modified;
    }

    public void setModified(double modified) {
        this.modified = modified;
    }
    /*@XmlElement(defaultValue = "Rate")
    public String getRate() {
        return rate;
    }
*/
    public void setRate(String rate) {
        this.rate = rate;
    }
}
