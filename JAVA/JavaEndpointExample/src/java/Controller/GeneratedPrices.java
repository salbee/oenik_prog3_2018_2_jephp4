package Controller;

import Model.Price;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Geri
 */
@XmlRootElement(name = "Business")
public class GeneratedPrices {
    @XmlElement 
    String Marka;
    @XmlElement 
    String Modell;
    @XmlElement 
    List<Price> prices;   
    
    public GeneratedPrices() {
    }
    
    
    
    public GeneratedPrices(int original, String Marka, String Modell) {
        this.prices = generatePrices(original);
        this.Marka=Marka;
        this.Modell=Modell;
    }
    
    private List<Price> generatePrices(double original){
        ArrayList<Price> local = new ArrayList<>();

        for (double i = 0.5; i < 1.5; i+=0.1) {
            Price p1 = new Price((int) original, i);
            local.add(p1);
        }
        return local;
    }
}