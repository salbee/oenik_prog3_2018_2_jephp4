﻿CREATE TABLE [dbo].[Kapcsolo]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Modell_id] INT NOT NULL, 
    [Extra_id] INT NOT NULL, 
    CONSTRAINT [FK_Modell] FOREIGN KEY ([Modell_id]) REFERENCES dbo.Modellek (Id),
	CONSTRAINT [FK_Extra] FOREIGN KEY (Extra_id) REFERENCES dbo.Extra (Id)
)
