namespace CarShop
{
    using System.Data.Entity;


    public partial class CarShopModell : DbContext

    {

        public CarShopModell()

            : base("name=CharShopModell")
        {
        }


        public virtual DbSet<AutoMarkak> AutoMarkak { get; set; }

        public virtual DbSet<Extra> Extra { get; set; }

        public virtual DbSet<Kapcsolo> Kapcsolo { get; set; }

        public virtual DbSet<Modellek> Modellek { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)

        {
            modelBuilder.Entity<AutoMarkak>()
                .Property(e => e.Nev)
                .IsUnicode(false);

            modelBuilder.Entity<AutoMarkak>()
                .Property(e => e.Orszag)
                .IsUnicode(false);

            modelBuilder.Entity<AutoMarkak>()
                .Property(e => e.URL)
                .IsFixedLength();

            modelBuilder.Entity<AutoMarkak>()
                .HasMany(e => e.Modellek)
                .WithRequired(e => e.AutoMarkak)
                .HasForeignKey(e => e.Marka_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Extra>()
                .Property(e => e.KategoriaNev)
                .IsFixedLength();

            modelBuilder.Entity<Extra>()
                .Property(e => e.Nev)
                .IsFixedLength();

            modelBuilder.Entity<Extra>()
                .Property(e => e.Szin)
                .IsUnicode(false);

            modelBuilder.Entity<Extra>()
                .Property(e => e.Hasznalhato)
                .IsFixedLength();

            modelBuilder.Entity<Extra>()
                .HasMany(e => e.Kapcsolo)
                .WithRequired(e => e.Extra)
                .HasForeignKey(e => e.Extra_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Modellek>()
                .Property(e => e.Nev)
                .IsUnicode(false);

            modelBuilder.Entity<Modellek>()
                .HasMany(e => e.Kapcsolo)
                .WithRequired(e => e.Modellek)
                .HasForeignKey(e => e.Modell_id)
                .WillCascadeOnDelete(false);
        }
    }
}
