namespace CarShop
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Extra")]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Extra'
    public partial class Extra
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Extra'
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Extra.Extra()'
        public Extra()
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Extra.Extra()'
        {
            Kapcsolo = new HashSet<Kapcsolo>();
        }

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Extra.Id'
        public int Id { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Extra.Id'

        [StringLength(10)]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Extra.KategoriaNev'
        public string KategoriaNev { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Extra.KategoriaNev'

        [StringLength(10)]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Extra.Nev'
        public string Nev { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Extra.Nev'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Extra.Ar'
        public int? Ar { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Extra.Ar'

        [StringLength(50)]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Extra.Szin'
        public string Szin { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Extra.Szin'

        [MaxLength(50)]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Extra.Hasznalhato'
        public byte[] Hasznalhato { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Extra.Hasznalhato'

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        
        public virtual ICollection<Kapcsolo> Kapcsolo { get; set; }

    }
}
