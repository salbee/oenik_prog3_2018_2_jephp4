namespace CarShop
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Kapcsolo")]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Kapcsolo'
    public partial class Kapcsolo
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Kapcsolo'
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Kapcsolo.Id'
        public int Id { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Kapcsolo.Id'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Kapcsolo.Modell_id'
        public int Modell_id { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Kapcsolo.Modell_id'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Kapcsolo.Extra_id'
        public int Extra_id { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Kapcsolo.Extra_id'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Kapcsolo.Extra'
        public virtual Extra Extra { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Kapcsolo.Extra'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Kapcsolo.Modellek'
        public virtual Modellek Modellek { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Kapcsolo.Modellek'
    }
}
