namespace CarShop
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AutoMarkak")]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak'
    public partial class AutoMarkak
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak'
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.AutoMarkak()'
        public AutoMarkak()
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.AutoMarkak()'
        {
            Modellek = new HashSet<Modellek>();
        }

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.Id'
        public int Id { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.Id'

        [StringLength(50)]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.Nev'
        public string Nev { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.Nev'

        [StringLength(50)]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.Orszag'
        public string Orszag { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.Orszag'

        [StringLength(10)]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.URL'
        public string URL { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.URL'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.Alapitas'
        public int? Alapitas { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.Alapitas'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.Forgalom'
        public int? Forgalom { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.Forgalom'

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.Modellek'
        public virtual ICollection<Modellek> Modellek { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'AutoMarkak.Modellek'
    }
}
