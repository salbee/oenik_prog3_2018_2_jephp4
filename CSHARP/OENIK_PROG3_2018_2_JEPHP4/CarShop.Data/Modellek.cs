namespace CarShop
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Modellek")]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Modellek'
    public partial class Modellek
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Modellek'
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Modellek()'
        public Modellek()
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Modellek()'
        {
            Kapcsolo = new HashSet<Kapcsolo>();
        }

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Id'
        public int Id { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Id'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Marka_id'
        public int Marka_id { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Marka_id'

        [Required]
        [StringLength(50)]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Nev'
        public string Nev { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Nev'

        [Column(TypeName = "date")]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Megjelenes'
        public DateTime? Megjelenes { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Megjelenes'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Motor'
        public int? Motor { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Motor'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Loero'
        public int? Loero { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Loero'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Ar'
        public int? Ar { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Ar'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Modellek.AutoMarkak'
        public virtual AutoMarkak AutoMarkak { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Modellek.AutoMarkak'

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Kapcsolo'
        public virtual ICollection<Kapcsolo> Kapcsolo { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'Modellek.Kapcsolo'
    }
}
