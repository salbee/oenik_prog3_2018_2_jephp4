﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CarShop.Repo

{
/// <summary>
/// Repository Interface
/// </summary>
/// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity>  

    {
       
        //CarShopModell CM { get; set; }

            /// <summary>
            /// Insert Interface
            /// </summary>
            /// <param name="NewEntity">New item</param>
            /// <param name="CM">Database</param>
        void Insert(TEntity NewEntity, CarShopModell CM);
        /// <summary>
        /// Delete Interface
        /// </summary>
        /// <param name="DeletEntity">Delete item</param>
        /// <param name="CM">Database</param>
        void Delete(TEntity DeletEntity, CarShopModell CM);
        /// <summary>
        /// Update Interface
        /// </summary>
        /// <param name="UpDateEntity">Update item</param>
        /// <param name="CM">Databasse</param>
        void UpDate(TEntity UpDateEntity, CarShopModell CM);
        /// <summary>
        /// Read Interface
        /// </summary>
        /// <param name="CM">Database</param>
        /// <returns>List</returns>
        List<TEntity>  Read(CarShopModell CM);
        /// <summary>
        /// Getall
        /// </summary>
        /// <param name="CM">Database</param>
        /// <returns>List</returns>
        IQueryable<TEntity> GetALL(CarShopModell CM);

    }
}
