﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarShop.Repo
{
/// <summary>
/// Model Repository
/// </summary>
    public class ModellRepo : IRepository<Modellek>

    {
        //public CarShopModell CM { get; set; }

        /// <summary>
        /// Delete item
        /// </summary>
        /// <param name="DeletEntity">item you want to delete</param>
        /// <param name="CM">Database</param>
        public void Delete(Modellek DeletEntity, CarShopModell CM)

        {            
            CM.Modellek.Remove(CM.Modellek.Where(d => d.Id == DeletEntity.Id).FirstOrDefault());
            CM.SaveChanges();
        }
        /// <summary>
        /// new item record
        /// </summary>
        /// <param name="NewEntity">New item</param>
        /// <param name="CM">Database</param>
        public void Insert(Modellek NewEntity, CarShopModell CM)

        {
            CM.Modellek.Add(NewEntity);
            CM.SaveChanges();
        }
/// <summary>
/// Query
/// </summary>
/// <param name="CM">Database</param>
/// <returns>List</returns>
        public IQueryable<Modellek> GetALL(CarShopModell CM)

        {
            return CM.Modellek.AsQueryable<Modellek>();
        }
        /// <summary>
        /// Read Model table
        /// </summary>
        /// <param name="CM">Database</param>
        /// <returns></returns>
        public List<Modellek> Read(CarShopModell CM)

        {            
            var Modellek= CM.Set<Modellek>().Select(d => d);
            return Modellek.ToList();
        }

        /// <summary>
        /// Update model table
        /// </summary>
        /// <param name="UpDateEntity">new data to the old place</param>
        /// <param name="CM">Database</param>
        public void UpDate(Modellek UpDateEntity, CarShopModell CM)
        {
            if(UpDateEntity.Nev!="")
            {
                CM.Set<Modellek>().Where(d => d.Id == UpDateEntity.Id).Single<Modellek>().Nev = UpDateEntity.Nev;
            }
            if (UpDateEntity.Megjelenes != null)
            {
                CM.Set<Modellek>().Where(d => d.Id == UpDateEntity.Id).Single<Modellek>().Megjelenes = UpDateEntity.Megjelenes;
            }
            if (UpDateEntity.Motor != null)
            {
                CM.Set<Modellek>().Where(d => d.Id == UpDateEntity.Id).Single<Modellek>().Motor = UpDateEntity.Motor;
            }
            if (UpDateEntity.Loero != null)
            {
                CM.Set<Modellek>().Where(d => d.Id == UpDateEntity.Id).Single<Modellek>().Loero = UpDateEntity.Loero;
            }
            if (UpDateEntity.Ar != null)
            {
                CM.Set<Modellek>().Where(d => d.Id == UpDateEntity.Id).Single<Modellek>().Ar = UpDateEntity.Ar;
            }
                CM.SaveChanges();
        }
        /// <summary>
        /// All extra listed
        /// </summary>
        /// <param name="CM">Database</param>
        public void ModellExtra(CarShopModell CM)

        {            
            var Kapcs = from M in CM.Modellek
                        join K in CM.Kapcsolo on M.Id equals K.Modell_id
                        select new { ENev = K.Extra.KategoriaNev, EAr = K.Extra.Ar, Modellnev = M.Nev, ModellAlapAr = M.Ar };
            foreach (var item in Kapcs)
            {                
                Console.WriteLine("Modell neve: " + item.Modellnev + " Alap ár: " + item.ModellAlapAr + "M Extra megnevezése: " + item.ENev + " Ára: " + item.EAr + "M");
            }
        }
        /// <summary>
        /// average prices of models
        /// </summary>
        /// <param name="CM">Database</param>
        public void AtlagAr(CarShopModell CM)

        {
            var AtlagAr = from Mo in CM.Modellek
                          group Mo by Mo.Marka_id into g
                          join AM in CM.AutoMarkak on g.Key equals AM.Id
                          select new { AtlagAr = g.Average(x => x.Ar), Marka = AM.Nev };
            foreach (var item in AtlagAr)
            {
                Console.WriteLine("Márka: " +item.Marka + " Átlag ára: " + item.AtlagAr + " M");
            }
        }
    }
}
