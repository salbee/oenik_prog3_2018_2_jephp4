﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarShop.Repo
{
/// <summary>
/// 
/// </summary>
    public class ExtraRepo : IRepository<Extra>

    {
        /// <summary>
        /// Delete item
        /// </summary>
        /// <param name="DeletEntity">item you want to delete</param>
        /// <param name="CM">Database</param>
        public void Delete(Extra DeletEntity, CarShopModell CM)

        {
            CM.Extra.Remove(CM.Extra.Where(d => d.Id == DeletEntity.Id).FirstOrDefault());
            CM.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CM">Databese</param>
        /// <returns>List</returns>
        public IQueryable<Extra> GetALL(CarShopModell CM)

        {
            return CM.Extra.AsQueryable<Extra>();
        }

        /// <summary>
        /// new item record
        /// </summary>
        /// <param name="NewEntity">New item</param>
        /// <param name="CM">Database</param>
        public void Insert(Extra NewEntity, CarShopModell CM)

        {
            CM.Extra.Add(NewEntity);
            CM.SaveChanges();
        }
        /// <summary>
        /// Read extra table
        /// </summary>
        /// <param name="CM">Database</param>
        /// <returns>Extra items List</returns>

        public List<Extra> Read(CarShopModell CM)

        {
            var Extra = CM.Set<Extra>().Select(d => d);
            return Extra.ToList();
        }

        /// <summary>
        /// Update Extra table
        /// </summary>
        /// <param name="UpDateEntity">Update item</param>
        /// <param name="CM">Database</param>
        public void UpDate(Extra UpDateEntity, CarShopModell CM)

        {
            if (UpDateEntity.KategoriaNev != null)
            {
                CM.Set<Extra>().Where(d => d.Id == UpDateEntity.Id).Single<Extra>().KategoriaNev = UpDateEntity.KategoriaNev;
            }
            if (UpDateEntity.Ar != null)
            {
                CM.Set<Extra>().Where(d => d.Id == UpDateEntity.Id).Single<Extra>().Ar = UpDateEntity.Ar;
            }
            if (UpDateEntity.Szin != null)
            {
                CM.Set<Extra>().Where(d => d.Id == UpDateEntity.Id).Single<Extra>().Szin = UpDateEntity.Szin;
            }
            if (UpDateEntity.Hasznalhato != null)
            {
                CM.Set<Extra>().Where(d => d.Id == UpDateEntity.Id).Single<Extra>().Hasznalhato = UpDateEntity.Hasznalhato;
            }            
            CM.SaveChanges();
        }
    }
}
