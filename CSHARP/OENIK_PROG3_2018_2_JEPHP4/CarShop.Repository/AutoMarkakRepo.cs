﻿namespace CarShop.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using CarShop;

/// <summary>
/// Brand Repository
/// </summary>
    public class AutoMarkakRepo  :IRepository<AutoMarkak>   {



        /// <summary>
        /// Delete item
        /// </summary>
        /// <param name="DeletEntity">item you want to delete</param>
        /// <param name="CM">Database</param>
        public void Delete(AutoMarkak DeletEntity, CarShopModell CM)

        {                               
            CM.AutoMarkak.Remove(CM.AutoMarkak.Where(d => d.Id == DeletEntity.Id).FirstOrDefault());
            CM.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CM">Databese</param>
        /// <returns>List</returns>
        public IQueryable<AutoMarkak> GetALL(CarShopModell CM)

        {
            return CM.AutoMarkak.AsQueryable<AutoMarkak>();
        }

        /// <summary>
        /// new item record
        /// </summary>
        /// <param name="NewEntity">New item</param>
        /// <param name="CM">Database</param>
        public void Insert(AutoMarkak NewEntity, CarShopModell CM)

        {
            CM.AutoMarkak.Add(NewEntity);
            CM.SaveChanges();
        }
        /// <summary>
        /// Read brand table
        /// </summary>
        /// <param name="CM">Database</param>
        /// <returns>Brand items List</returns>
        public List<AutoMarkak> Read(CarShopModell CM)

        {
            
            var Auto= CM.Set<AutoMarkak>().Select(d => d);
            return Auto.ToList();
        }
        /// <summary>
        /// Update brand table
        /// </summary>
        /// <param name="UpDateEntity">new data to the old place</param>
        /// <param name="CM">Database</param>

        public void UpDate(AutoMarkak UpDateEntity, CarShopModell CM)

        {   
            if(UpDateEntity.Nev!="")
            {
                CM.Set<AutoMarkak>().Where(d => d.Id == UpDateEntity.Id).Single<AutoMarkak>().Nev = UpDateEntity.Nev;
            }
            if (UpDateEntity.Orszag != "")
            {
                CM.Set<AutoMarkak>().Where(d => d.Id == UpDateEntity.Id).Single<AutoMarkak>().Orszag = UpDateEntity.Orszag;
            }
            if (UpDateEntity.URL != "")
            {
                CM.Set<AutoMarkak>().Where(d => d.Id == UpDateEntity.Id).Single<AutoMarkak>().URL = UpDateEntity.URL;
            }
            if (UpDateEntity.Alapitas != null)
            {
                CM.Set<AutoMarkak>().Where(d => d.Id == UpDateEntity.Id).Single<AutoMarkak>().Alapitas = UpDateEntity.Alapitas;
            }
            if (UpDateEntity.Forgalom != null)
            {
                CM.Set<AutoMarkak>().Where(d => d.Id == UpDateEntity.Id).Single<AutoMarkak>().Forgalom = UpDateEntity.Forgalom;
            }
            CM.SaveChanges();
        }
        /// <summary>
        /// All models of the brand
        /// </summary>
        /// <param name="CM">Datebase</param>
        public void MarkaModell (CarShopModell CM)

        {
                var AM = from e in CM.Modellek
                         join a in CM.AutoMarkak on e.Marka_id equals a.Id                         
                         select new { Marka = a.Nev, Modell = e.Nev };
            foreach (var item in AM)
            {
                Console.WriteLine(item.Marka + " Modell: " + item.Modell);
            }
            //return AM.ToList();
        }
        /// <summary>
        /// Java endpoint
        /// </summary>
        /// <param name="Marka">Name of brand</param>
        /// <param name="Modell">Name of model</param>
        /// <param name="Ar">Price</param>
        public void Java(string Marka,string Modell, int Ar)

        {

            string url = "http://localhost:8080/Shop/Price?marka=" + Marka + "&modell=" + Modell + "&price=" + Ar;
            try
            {
                XDocument doc = XDocument.Load(url);
                var ajanlat = from x in doc.Descendants("prices")
                              select new { Arak = x.Element("offer").Value };
                Console.WriteLine("Ajánlatok a" + Marka + " " + Modell + "-re");
                foreach (var item in ajanlat)
                {
                    Console.WriteLine(item.Arak);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(url+" "+e.Message);
                Console.WriteLine(" Nem indult el a webserver");
            }
            
            
        }
    }
}
