var namespace_car_shop =
[
    [ "Logic", "namespace_car_shop_1_1_logic.html", "namespace_car_shop_1_1_logic" ],
    [ "Repo", "namespace_car_shop_1_1_repo.html", "namespace_car_shop_1_1_repo" ],
    [ "Test", "namespace_car_shop_1_1_test.html", "namespace_car_shop_1_1_test" ],
    [ "AutoMarkak", "class_car_shop_1_1_auto_markak.html", "class_car_shop_1_1_auto_markak" ],
    [ "CarShopModell", "class_car_shop_1_1_car_shop_modell.html", "class_car_shop_1_1_car_shop_modell" ],
    [ "Extra", "class_car_shop_1_1_extra.html", "class_car_shop_1_1_extra" ],
    [ "Kapcsolo", "class_car_shop_1_1_kapcsolo.html", "class_car_shop_1_1_kapcsolo" ],
    [ "LogicAuto", "class_car_shop_1_1_logic_auto.html", "class_car_shop_1_1_logic_auto" ],
    [ "Modellek", "class_car_shop_1_1_modellek.html", "class_car_shop_1_1_modellek" ],
    [ "Program", "class_car_shop_1_1_program.html", null ],
    [ "RepositoryTest", "class_car_shop_1_1_repository_test.html", null ]
];