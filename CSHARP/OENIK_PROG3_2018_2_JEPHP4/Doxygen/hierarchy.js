var hierarchy =
[
    [ "CarShop.AutoMarkak", "class_car_shop_1_1_auto_markak.html", null ],
    [ "CarShop.Test.CarShopTest", "class_car_shop_1_1_test_1_1_car_shop_test.html", null ],
    [ "DbContext", null, [
      [ "CarShop.CarShopModell", "class_car_shop_1_1_car_shop_modell.html", null ]
    ] ],
    [ "CarShop.Extra", "class_car_shop_1_1_extra.html", null ],
    [ "CarShop.Logic.ILogic< T >", "interface_car_shop_1_1_logic_1_1_i_logic.html", null ],
    [ "CarShop.Logic.ILogic< AutoMarkak >", "interface_car_shop_1_1_logic_1_1_i_logic.html", [
      [ "CarShop.LogicAuto", "class_car_shop_1_1_logic_auto.html", null ]
    ] ],
    [ "CarShop.Logic.ILogic< Extra >", "interface_car_shop_1_1_logic_1_1_i_logic.html", [
      [ "CarShop.Logic.LogicExtra", "class_car_shop_1_1_logic_1_1_logic_extra.html", null ]
    ] ],
    [ "CarShop.Logic.ILogic< Modellek >", "interface_car_shop_1_1_logic_1_1_i_logic.html", [
      [ "CarShop.Logic.LogicModell", "class_car_shop_1_1_logic_1_1_logic_modell.html", null ]
    ] ],
    [ "CarShop.Repo.IRepository< TEntity >", "interface_car_shop_1_1_repo_1_1_i_repository.html", null ],
    [ "CarShop.Repo.IRepository< AutoMarkak >", "interface_car_shop_1_1_repo_1_1_i_repository.html", [
      [ "CarShop.Repo.AutoMarkakRepo", "class_car_shop_1_1_repo_1_1_auto_markak_repo.html", null ]
    ] ],
    [ "CarShop.Repo.IRepository< Extra >", "interface_car_shop_1_1_repo_1_1_i_repository.html", [
      [ "CarShop.Repo.ExtraRepo", "class_car_shop_1_1_repo_1_1_extra_repo.html", null ]
    ] ],
    [ "CarShop.Repo.IRepository< Modellek >", "interface_car_shop_1_1_repo_1_1_i_repository.html", [
      [ "CarShop.Repo.ModellRepo", "class_car_shop_1_1_repo_1_1_modell_repo.html", null ]
    ] ],
    [ "CarShop.Kapcsolo", "class_car_shop_1_1_kapcsolo.html", null ],
    [ "CarShop.Modellek", "class_car_shop_1_1_modellek.html", null ],
    [ "CarShop.Program", "class_car_shop_1_1_program.html", null ],
    [ "CarShop.RepositoryTest", "class_car_shop_1_1_repository_test.html", null ]
];