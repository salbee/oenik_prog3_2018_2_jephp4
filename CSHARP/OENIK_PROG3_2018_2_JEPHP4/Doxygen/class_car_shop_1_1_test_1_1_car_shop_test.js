var class_car_shop_1_1_test_1_1_car_shop_test =
[
    [ "Atlagar", "class_car_shop_1_1_test_1_1_car_shop_test.html#a413e5027de0223c17d487ac66ac62e37", null ],
    [ "DeleteAuto", "class_car_shop_1_1_test_1_1_car_shop_test.html#ae6ebc6e17015e5bccdf8d21989dbf425", null ],
    [ "DeleteExtra", "class_car_shop_1_1_test_1_1_car_shop_test.html#a7dab67b1f690e7df0968d5a43b689424", null ],
    [ "DeleteModell", "class_car_shop_1_1_test_1_1_car_shop_test.html#aabf5788b85ef2172133667721fabe8d7", null ],
    [ "InsertAuto", "class_car_shop_1_1_test_1_1_car_shop_test.html#a4a2efcee45538cb5558099e2d932a3c8", null ],
    [ "InsertExtra", "class_car_shop_1_1_test_1_1_car_shop_test.html#a22c8bd693f88b1fec769e7e72303d3d0", null ],
    [ "InsertModell", "class_car_shop_1_1_test_1_1_car_shop_test.html#a4f93cc763550c97463113f7d10957b3a", null ],
    [ "MarkaModell", "class_car_shop_1_1_test_1_1_car_shop_test.html#a198f239082c6034387e21cf8eab6b079", null ],
    [ "ModellList", "class_car_shop_1_1_test_1_1_car_shop_test.html#a64c6d97e0dd7287cd6f5845984380e00", null ],
    [ "ReadAuto", "class_car_shop_1_1_test_1_1_car_shop_test.html#a9a2516c1d86e82f706c88743061402c6", null ],
    [ "ReadExtra", "class_car_shop_1_1_test_1_1_car_shop_test.html#aa3d8ce9e67e174e4d9c93d44d270a280", null ],
    [ "ReadModell", "class_car_shop_1_1_test_1_1_car_shop_test.html#ada4910cd76ed4e3eb90b33664fac8b94", null ],
    [ "Setup", "class_car_shop_1_1_test_1_1_car_shop_test.html#a32e3a2a811d857f426aaa228be715f54", null ],
    [ "UpdateAuto", "class_car_shop_1_1_test_1_1_car_shop_test.html#adc295f09781a6c9864e5559d00ade74a", null ],
    [ "UpdateExtra", "class_car_shop_1_1_test_1_1_car_shop_test.html#aff11701efb571f8025ff2583eef259ec", null ],
    [ "UpdateModell", "class_car_shop_1_1_test_1_1_car_shop_test.html#a54f8998947413d066fa51a1671489aea", null ]
];