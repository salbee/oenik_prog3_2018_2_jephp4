var dir_8f214850cca0cd16d0732b8e94b2d184 =
[
    [ "CarShop.Data", "dir_ec30291942eba775d390f75396a0bab1.html", "dir_ec30291942eba775d390f75396a0bab1" ],
    [ "CarShop.Logic", "dir_f4f490cee3485ec290106390eb343549.html", "dir_f4f490cee3485ec290106390eb343549" ],
    [ "CarShop.Logic.Test", "dir_e8eabeae3033716d28e122ce9c6ae0d3.html", "dir_e8eabeae3033716d28e122ce9c6ae0d3" ],
    [ "CarShop.Program", "dir_a74f84f144f1c972f8b805fdd7b29acd.html", "dir_a74f84f144f1c972f8b805fdd7b29acd" ],
    [ "CarShop.Repository", "dir_3bf25f341f2edb2e03def9864befd00d.html", "dir_3bf25f341f2edb2e03def9864befd00d" ],
    [ "CarShop.Repository.Tests", "dir_93fc7b25eff863c694880c24e7ee9cd7.html", "dir_93fc7b25eff863c694880c24e7ee9cd7" ]
];