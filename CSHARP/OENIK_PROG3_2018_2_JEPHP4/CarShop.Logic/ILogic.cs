﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarShop.Logic
{
    interface ILogic<T>
    {
        List<T> Read();
        void Insert(T item);
        void Delete(T item);
        void UpDate(T item);
    }
}
