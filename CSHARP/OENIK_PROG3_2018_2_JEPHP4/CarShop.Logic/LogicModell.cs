﻿using CarShop.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarShop.Logic
{
    /// <summary>
    /// Model Business logic
    /// </summary>
    public class LogicModell : ILogic<Modellek>

    {
        private ModellRepo ModellRepo = new ModellRepo();
        static CarShopModell CM;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'LogicModell.LogicModell(CarShopModell)'
        public LogicModell(CarShopModell cM)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'LogicModell.LogicModell(CarShopModell)'

        {
            CM = cM;
        }
        /// <summary>
        /// Delete method call the repository delete method
        /// </summary>
        /// <param name="item">Deleted item</param>
        public void Delete(Modellek item)

        {
            ModellRepo.Delete(item, CM);
        }

        /// <summary>
        /// Insert method call the repository insert method
        /// </summary>
        /// <param name="item">Insert item</param>
        public void Insert(Modellek item)

        {
            ModellRepo.Insert(item, CM);
        }
        /// <summary>
        /// Call the Update Repository 
        /// </summary>
        /// <param name="item">Update item</param>
        public void UpDate(Modellek item )

        {
            ModellRepo.UpDate(item, CM);
        }
        /// <summary>
        ///call the average prices of models
        /// </summary>
        public void AtlagAr()

        {
            ModellRepo.AtlagAr(CM);
        }
/// <summary>
/// Call the List of extras
/// </summary>
        public void  ModellExtra()

        {
            ModellRepo.ModellExtra(CM);
        }
        /// <summary>
        /// Call the Read repository and Write some information
        /// </summary>
        /// <returns>List</returns>
        public List<Modellek> Read()

        {
            List<Modellek> ModellLista = ModellRepo.Read(CM);
            foreach (var item in ModellLista)
            {
                Console.Write(item.Id + " ");
                Console.Write(item.Nev + " ");
                Console.Write(item.Loero + " ");
                Console.Write(item.Motor + " ");
                Console.Write(item.Ar + "\n");
            }
            return ModellLista;
        }
    }
}
