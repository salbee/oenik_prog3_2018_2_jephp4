﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarShop;
using CarShop.Logic;
using CarShop.Repo;

namespace CarShop
{
    /// <summary>
    /// Brand Business logic
    /// </summary>
    public class LogicAuto : ILogic<AutoMarkak>

    {        
        private AutoMarkakRepo AutoRepo=new AutoMarkakRepo();        
         CarShopModell CM;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'LogicAuto.LogicAuto(CarShopModell)'
        public LogicAuto(CarShopModell cM)        {
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'LogicAuto.LogicAuto(CarShopModell)'
            CM = cM;
        }
        /// <summary>
        /// Call the Read repository and Write some information
        /// </summary>
        /// <returns>List</returns>
        public List<AutoMarkak> Read()

        {               
            List<AutoMarkak> AutoLista = AutoRepo.Read(CM);
            foreach (var item in AutoLista)
            {
                Console.Write(item.Id+" ");
                Console.Write(item.Nev+" ");                
                Console.Write(item.Alapitas + " ");
                Console.Write(item.Orszag+" ");
                Console.Write(item.Forgalom + "\n");
            }
            return AutoLista;
            
        }
        /// <summary>
        /// Collect the data from the java endpoint
        /// </summary>
        public void JavaL()

        {
            try
            {
                Console.Write("Milyen márkárá kér ajánlatott: ");
                string marka = Convert.ToString(Console.ReadLine());
                Console.Write("Milyen modellre kér ajánlatott: ");
                string modell = Console.ReadLine();
                Console.Write("Milyen áron: ");
                int ar = Convert.ToInt32(Console.ReadLine());
                AutoRepo.Java(marka, modell, ar);
            }
            catch

            {
                Console.WriteLine("Rossz adat");
            }            
            
        }

        /// <summary>
        /// Insert method call the repository insert method
        /// </summary>
        /// <param name="item">Insert item</param>
        public void Insert(AutoMarkak item)
        {
            AutoRepo.Insert(item, CM);
        }
        /// <summary>
        /// Delete method call the repository delete method
        /// </summary>
        /// <param name="item">Deleted item</param>
        public void Delete(AutoMarkak item)

        {           
            AutoRepo.Delete(item, CM);
        }
        /// <summary>
        /// Call the Update Repository 
        /// </summary>
        /// <param name="item">Update item</param>
        public void UpDate(AutoMarkak item)

        {
            AutoRepo.UpDate(item, CM);
        }
        /// <summary>
        /// Call the model list method
        /// </summary>
        public void MarkaModellLista()

        {
            AutoRepo.MarkaModell(CM);
        }
       
    }


}
