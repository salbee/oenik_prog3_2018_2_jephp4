﻿using CarShop.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarShop.Logic
{
    /// <summary>
    /// Extra Business logic
    /// </summary>
    public class LogicExtra : ILogic<Extra>

    {
        private ExtraRepo ExtraRepo = new ExtraRepo();
        static CarShopModell CM;
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'LogicExtra.LogicExtra(CarShopModell)'
        public LogicExtra(CarShopModell cM)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'LogicExtra.LogicExtra(CarShopModell)'

        {
            CM = cM;
        }

        /// <summary>
        /// Delete method call the repository delete method
        /// </summary>
        /// <param name="item">Deleted item</param>
        public void Delete(Extra item)

        {
            ExtraRepo.Delete(item, CM);
        }

        /// <summary>
        /// Insert method call the repository insert method
        /// </summary>
        /// <param name="item">Insert item</param>
        public void Insert(Extra item)

        {
            ExtraRepo.Insert(item, CM);
        }
        /// <summary>
        /// Call the Read repository and Write some information
        /// </summary>
        /// <returns>List</returns>
        public List<Extra> Read()

        {
            List<Extra> ExtraLista = ExtraRepo.Read(CM);
            foreach (var item in ExtraLista)
            {
                Console.Write(item.Id + " ");                
                Console.Write(item.KategoriaNev + " ");
                Console.Write(item.Ar+"\n");
            }
            return ExtraLista;
        }
        /// <summary>
        /// Call the Update Repository 
        /// </summary>
        /// <param name="item">Update item</param>
        public void UpDate(Extra item)

        {
            ExtraRepo.UpDate(item,CM);
            
        }
    }
}
