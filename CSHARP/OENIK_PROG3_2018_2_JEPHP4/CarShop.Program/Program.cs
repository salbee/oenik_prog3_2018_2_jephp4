﻿using CarShop.Logic;
using System;
using System.Collections.Generic;


namespace CarShop
{
    class Program
    {
        static void Main(string[] args)
        {
            CarShopModell CM = new CarShopModell();
            LogicAuto AutoMLogic = new LogicAuto(CM);
            LogicModell ModellLogic = new LogicModell(CM);
            LogicExtra ExtraLogic = new LogicExtra(CM);
            AutoMarkak MarkaI;
            Modellek ModellI;
            Extra ExtraI;
            Console.WriteLine("Adatbázis kapcsolat kész!\n");
            bool rendben = false;
            do
            {             
                
                Console.WriteLine("Válaszz az alábbiak közül:");
                Console.WriteLine("1 -- Autó tábla olvasása");
                Console.WriteLine("2 -- Új autómárka felvétele");
                Console.WriteLine("3 -- Autó tábla frissítése");
                Console.WriteLine("4 -- Törlés az autó táblából");
                Console.WriteLine("5 -- Ajánlat kérés");
                Console.WriteLine("6 -- Márkákhoz tartozó modell");
                Console.WriteLine("7 -- Modellekhez tartozó extra");
                Console.WriteLine("8 -- Márkák átlag árai");
                Console.WriteLine("9 -- Modell tábla olvasása");
                Console.WriteLine("10 -- Új modell felvétele");
                Console.WriteLine("11 -- Modell tábla frissítése");
                Console.WriteLine("12 -- Törlés a modell táblából");
                Console.WriteLine("13 -- Extrák tábla olvasása");
                Console.WriteLine("14 -- Új extra felvétele");
                Console.WriteLine("15 -- Extra tábla frissítése");
                Console.WriteLine("16 -- Törlés az extra táblából");
                Console.Write("A választott szám + ENTER! (Kilépés: 0): ");
                string strValasztas = Console.ReadLine();
                int iEzkell;
                try
                {
                    Console.Clear();
                    iEzkell = int.Parse(strValasztas);
                }
                catch (FormatException)
                {
                    Console.WriteLine("HIBA rossz megadott menüpont");
                    continue;
                }             


                switch (iEzkell)
                {
                    case 0:
                        rendben = true;
                        break;
                    case 1:
                        Console.Clear();
                        AutoMLogic.Read();
                        break;
                    case 2:
                        Console.Clear();
                        MarkaI = new AutoMarkak();
                        AutoMLogic.Insert(MarkaBeker(MarkaI));
                        break;
                    case 3:
                        Console.Clear();
                        AutoMLogic.Read();
                        Console.Write("Melyik sorszámú elemet akarja modosítani: ");
                        MarkaI = new AutoMarkak();
                        try
                        {
                            MarkaI.Id = Convert.ToInt32(Console.ReadLine());
                            AutoMLogic.UpDate(MarkaBeker(MarkaI));                           
                        }catch
                        {
                            Console.WriteLine("Rossz adat");
                        }
                        
                        break;
                    case 4:
                        Console.Clear();
                        MarkaI = new AutoMarkak();
                        AutoMLogic.Read();
                        Console.Write("Melyik sorszámú elemet akarja törölni:");
                        try
                        {
                            MarkaI.Id = Convert.ToInt32(Console.ReadLine());
                            AutoMLogic.Delete(MarkaI);
                        }
                        catch
                        {
                            Console.WriteLine("Rossz adat");
                        }                        
                        break;
                    case 5:
                        Console.Clear();
                        AutoMLogic.JavaL();
                        break;
                    case 6:
                        Console.Clear();
                        AutoMLogic.MarkaModellLista();
                        break;
                    case 7:
                        Console.Clear();
                        ModellLogic.ModellExtra();
                        break;
                    case 8:
                        Console.Clear();
                        ModellLogic.AtlagAr();
                        break;
                    case 9:
                        Console.Clear();
                        ModellLogic.Read();
                        break;
                    case 10:
                        Console.Clear();
                        AutoMLogic.Read();
                        Console.Write("Melyik sorzsámú márkához tartozik: ");
                        ModellI = new Modellek();
                        try
                        {
                            ModellI.Marka_id = Convert.ToInt32(Console.ReadLine());
                            ModellLogic.Insert(ModellBeker(ModellI));

                        }
                        catch
                        {
                            Console.WriteLine("Rossz adat");
                        }
                        
                        break;
                    case 11:
                        Console.Clear();
                        ModellLogic.Read();
                        ModellI = new Modellek();
                        Console.Write("Melyik sorszámú elemet akarja modosítani: ");
                        try
                        {
                            ModellI.Id = Convert.ToInt32(Console.ReadLine());
                            ModellLogic.UpDate(ModellBeker(ModellI));
                        }catch
                        {
                            Console.WriteLine("Rossz adat");
                        }                        
                        break;
                    case 12:
                        Console.Clear();
                        ModellLogic.Read();
                        ModellI = new Modellek();
                        Console.Write("Melyik sorszámú elemet akarja törölni: ");
                        try
                        {
                            ModellI.Id = Convert.ToInt32(Console.ReadLine());
                            ModellLogic.Delete(ModellI);
                        }
                        catch
                        {
                            Console.WriteLine("Rossz adat");
                        }
                        
                        break;
                    case 13:
                        Console.Clear();
                        ExtraLogic.Read();
                        break;
                    case 14:
                        Console.Clear();
                        ExtraI = new Extra();
                        ExtraLogic.Insert(ExtraBeker(ExtraI));
                        break;
                    case 15:
                        Console.Clear();
                        ExtraI = new Extra();
                        ExtraLogic.Read();
                        Console.Write("Melyik sorszámú elemet akarja modosítani: ");
                        try
                        {
                            ExtraI.Id = Convert.ToInt32(Console.ReadLine());
                            ExtraLogic.UpDate(ExtraBeker(ExtraI));
                        }
                        catch
                        {
                            Console.WriteLine("Rossz adat");
                        }                                              
                        break;
                    case 16:
                        Console.Clear();
                        ExtraLogic.Read();
                        ExtraI = new Extra();
                        Console.Write("Melyik sort szeretné törölni: ");
                        try
                        {
                            ExtraI.Id = Convert.ToInt32(Console.ReadLine());
                            ExtraLogic.Delete(ExtraI);
                        }
                        catch
                        {
                            Console.WriteLine("Rossz adat");
                        }                        
                        break;                 

                    default:
                        Console.WriteLine("Ilyen számot nem választhatsz!: " + iEzkell);
                        continue;
                }

                Console.WriteLine();
            }
            while (!rendben);
            Console.Clear();
            Console.WriteLine("Viszlát!");
            Console.ReadLine();
        }
        static AutoMarkak MarkaBeker(AutoMarkak Markaitem)
        {                    
            try
            {
                Console.Write("Márka név: ");
                Markaitem.Nev = Convert.ToString(Console.ReadLine());
                Console.Write("Ország: ");
                Markaitem.Orszag = Convert.ToString(Console.ReadLine());
                Console.Write("URL: ");
                Markaitem.URL = Convert.ToString(Console.ReadLine());
                Console.Write("Forgalom: ");
                Markaitem.Forgalom = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Rossz adat");
            }
            return Markaitem;

        }
        static Modellek ModellBeker(Modellek Modellitem)
        {          
            try
            {                
                Console.Write("Modell név: ");
                Modellitem.Nev = Convert.ToString(Console.ReadLine());
                Console.Write("Lóerő: ");
                Modellitem.Loero = Convert.ToInt32(Console.ReadLine());
                Console.Write("Motor: ");
                Modellitem.Motor = Convert.ToInt32(Console.ReadLine());
                Console.Write("Ár: ");
                Modellitem.Ar = Convert.ToInt32(Console.ReadLine());
            }catch
            {
                Console.WriteLine("Rossz adat");
            }
            return Modellitem;
        }
        static Extra ExtraBeker(Extra Extraitem)
        {
            try
            {
                Extraitem = new Extra();
                Console.Write("Extra ára: ");
                Extraitem.Ar = Convert.ToInt32(Console.ReadLine());
                Console.Write("Extra neve: ");
                Extraitem.KategoriaNev = Convert.ToString(Console.ReadLine());
            }catch
            {
                Console.WriteLine("Rossz adat");
            }
           
            return Extraitem;
        }
    }
}
