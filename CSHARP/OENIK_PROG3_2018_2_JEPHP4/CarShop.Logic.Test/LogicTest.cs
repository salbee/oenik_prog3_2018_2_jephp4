﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CarShop.Logic;
using CarShop.Repo;
using Moq;
using NUnit.Framework;

namespace CarShop.Test
{
    [TestFixture]
    public class CarShopTest    {
#pragma warning disable CS0169 // The field 'CarShopTest.MockCS' is never used
        Mock<CarShopModell> MockCS;
#pragma warning restore CS0169 // The field 'CarShopTest.MockCS' is never used
#pragma warning disable CS0169 // The field 'CarShopTest.AutoMock' is never used
        Mock<AutoMarkakRepo> AutoMock;
#pragma warning restore CS0169 // The field 'CarShopTest.AutoMock' is never used
#pragma warning disable CS0169 // The field 'CarShopTest.ModellMock' is never used
        Mock<ModellRepo> ModellMock;
#pragma warning restore CS0169 // The field 'CarShopTest.ModellMock' is never used


        List<AutoMarkak> AutoSetup;
        List<Modellek> ModellSetup;
        CarShopModell CM = new CarShopModell();
        LogicAuto LogicAuto;
        LogicModell LogicModell;
        LogicExtra LogicExtra;



        [SetUp]

        public void Setup()

        {

            AutoSetup = new List<AutoMarkak>()
            {
                new AutoMarkak() {Id=0, Nev="Kia", Orszag="Korea", Alapitas=1988, Forgalom=10, URL="kia.hu"},
                new AutoMarkak() {Id=1, Nev="Fiat", Orszag="Olasz", Alapitas=1910, URL="fiat.hu", Forgalom=9 }
            };
            ModellSetup = new List<Modellek>()
            {
                new Modellek() {Id=1, Nev="Ceed", Loero=50,Ar=9,Marka_id=0, Motor=500 },
                new Modellek() {Id=2, Nev="Punto", Motor=2000, Ar=15,Marka_id=1,Loero=120}
            };
            //MockCS.Setup(x => x.AutoMarkak);            
            LogicAuto = new LogicAuto(CM);
            LogicExtra = new LogicExtra(CM);
            LogicModell = new LogicModell(CM);
            //LogicAuto = new LogicAuto(MockCS.Object); 
            // AutoMock.Setup(x => x.Read(CM)).Returns(AutoSetup);


        }
        [Test]

        public void ReadAuto()

        {
            Assert.IsNotEmpty(LogicAuto.Read());
            Assert.That(() => LogicAuto.Read(), Throws.Nothing);
        }
        [Test]

        public void ReadModell()

        {
            Assert.IsNotEmpty(LogicModell.Read());
            Assert.That(() => LogicModell.Read(), Throws.Nothing);
        }
        [Test]

        public void ReadExtra()

        {
            Assert.IsNotEmpty(LogicModell.Read());
            Assert.That(() => LogicExtra.Read(), Throws.Nothing);
        }
        [Test]

        public void InsertAuto()

        {
            AutoMarkak tmp = new AutoMarkak();
            tmp.Nev = "Mo";
            Assert.That(() => LogicAuto.Insert(tmp), Throws.Nothing);
            Assert.That(() => LogicAuto.Insert(null), Throws.Exception);
        }
        [Test]
        public void InsertModell()

        {
            Modellek tmp = new Modellek();
            tmp.Nev = "Mo";
            tmp.Marka_id = 1;
            Assert.That(() => LogicModell.Insert(tmp), Throws.Nothing);
            Assert.That(() => LogicModell.Insert(null), Throws.Exception);
        }
        [Test]
        public void InsertExtra()

        {
            Extra tmp = new Extra();
            tmp.Nev = "Mo";
            tmp.Ar = 9;
            Assert.That(() => LogicExtra.Insert(tmp), Throws.Nothing);
            Assert.That(() => LogicExtra.Insert(null), Throws.Exception);
        }
        [Test]

        public void DeleteAuto()

        {
            AutoMarkak tmp = new AutoMarkak();
            tmp.Id = 5;
            Assert.That(() => LogicAuto.Delete(tmp), Throws.Nothing);
            tmp.Id = 50;
            Assert.That(() => LogicAuto.Delete(tmp), Throws.Exception);
            Assert.That(() => LogicAuto.Delete(null), Throws.Exception);
        }
        [Test]

        public void DeleteModell()

        {
            Modellek tmp = new Modellek();
            tmp.Id = 2;
            tmp.Marka_id = 1;
            Assert.That(() => LogicModell.Delete(tmp), Throws.Nothing);
            Assert.That(() => LogicModell.Delete(null), Throws.Exception);
            tmp.Id = 88;
            Assert.That(() => LogicModell.Delete(tmp), Throws.Exception);
        }
        [Test]
        public void DeleteExtra()
        {
            Extra tmp = new Extra();
            tmp.Id = 3;
            tmp.Nev = "Mo";
            tmp.Ar = 9;
            Assert.That(() => LogicExtra.Delete(tmp), Throws.Nothing);
            tmp.Id = 55;
            Assert.That(() => LogicExtra.Delete(tmp), Throws.Exception);
            Assert.That(() => LogicExtra.Delete(null), Throws.Exception);

        }

        [Test]
        public void UpdateAuto()
        {
            AutoMarkak tmp = new AutoMarkak();
            tmp.Id = 5;
            tmp.Nev = "dsadasd";
            Assert.That(() => LogicAuto.UpDate(tmp), Throws.Nothing);            
            tmp.Id = 50;
            Assert.That(() => LogicAuto.UpDate(tmp), Throws.Exception);
            Assert.That(() => LogicAuto.UpDate(null), Throws.Exception);            
        }
        [Test]
        public void UpdateModell()
        {
            Modellek tmp = new Modellek();
            tmp.Id = 2;
            tmp.Nev = "asdas";
            tmp.Marka_id = 1;
            Assert.That(() => LogicModell.UpDate(tmp), Throws.Nothing);
            Assert.That(() => LogicModell.UpDate(null), Throws.Exception);
            tmp.Id = 88;
            Assert.That(() => LogicModell.UpDate(tmp), Throws.Exception);
        }
        [Test]
        public void UpdateExtra()
        {
            Extra tmp = new Extra();
            tmp.Id = 3;
            tmp.KategoriaNev = "Mo";
            tmp.Ar = 9;
            Assert.That(() => LogicExtra.UpDate(tmp), Throws.Nothing);
            tmp.Id = 55;
            Assert.That(() => LogicExtra.UpDate(tmp), Throws.Exception);
            Assert.That(() => LogicExtra.UpDate(null), Throws.Exception);
        }
        [Test]
        public void MarkaModell()
        {
            Assert.That(() => LogicAuto.MarkaModellLista(), Throws.Nothing);
        }
        
        [Test]

        public void Atlagar()

        {
            Assert.That(() => LogicModell.AtlagAr(), Throws.Nothing);
        }
        [Test]

        
        public void ModellList()

        {
            Assert.That(() => LogicModell.ModellExtra(), Throws.Nothing);
        }
    }
}
